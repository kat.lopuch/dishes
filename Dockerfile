FROM node:16-alpine
WORKDIR /app/dishes
COPY . .
RUN npm install --production
CMD ["npm", "start"]