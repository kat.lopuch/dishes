import { useAppDispatch } from "../../hooks/hooks";
import { clearForm } from "../../store/dishSlice";
import "./clearButton.css";

export default function ClearButton() {
  const dispatch = useAppDispatch();

  const handleClearBtnClick = () => {
    dispatch(clearForm());
  };

  return (
    <button className="clearButton__button" onClick={handleClearBtnClick}>
      Clear
    </button>
  );
}
