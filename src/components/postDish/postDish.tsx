import { useAppDispatch } from "../../hooks/hooks";
import { sendDishRequest } from "../../store/addedDishSlice";
import usePostDishRequest from "../../hooks/usePostDishRequest";
import usePostDishPending from "../../hooks/usePostDishPending";
import useAreDishInputsValid from "../../hooks/useAreDishInputsValid";
import Loader from "../loader/loader";
import "./postDish.css";

export default function PostDish() {
  const dispatch = useAppDispatch();
  const postDishRequest = usePostDishRequest();
  const postDishPending = usePostDishPending();
  const areDishInputsValid = useAreDishInputsValid();
  const isPostDishButtonDisabled = postDishPending || !areDishInputsValid;
  let className =
    "postDish__button " +
    (isPostDishButtonDisabled
      ? "postDish__button--disabled"
      : "postDish__button--enabled");

  const handleClick = () => {
    if (isPostDishButtonDisabled) return;
    dispatch(sendDishRequest(postDishRequest));
  };

  return (
    <button className={className} onClick={handleClick}>
      {postDishPending ? <Loader /> : "Add dish"}
    </button>
  );
}
