export default interface HexTileProps {
  label: string;
  value: string | undefined;
  even?: boolean;
}
