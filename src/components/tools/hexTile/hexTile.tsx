import HexTileProps from "./hexTile.type";
import "./hexTile.css";

export default function HexTile(props: HexTileProps) {
  return (
    <div className={props.even ? "hex-tile hex-tile--even" : "hex-tile"}>
      <div className="hex-tile--left"></div>
      <div className="hex-tile--middle">
        <p className="hex-tile__text">
          {props.label}: {props.value}
        </p>
      </div>
      <div className="hex-tile--right"></div>
    </div>
  );
}
