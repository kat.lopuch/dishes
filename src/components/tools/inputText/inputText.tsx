import InputTextProps from "./inputText.types";
import "./inputText.css";

export default function InputText(props: InputTextProps) {
  const onChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    props.onChange(event.target.value);

  const classErrorMsg = props.showError
    ? "inputText__error--shown"
    : "inputText__error--hidden";

  return (
    <div className="inputText__container">
      <label className="inputText__label">{props.label}</label>
      <input
        data-testid="input-text-id"
        type="text"
        className={props.className ? props.className : "inputText__input"}
        value={props.value}
        onChange={onChange}
        onBlur={props.onFocusedLost}
        placeholder={props.placeholder}
      ></input>
      <p data-testid="error-text-id" className={classErrorMsg}>
        {props.errorMessage}
      </p>
    </div>
  );
}
