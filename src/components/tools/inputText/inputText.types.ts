export default interface InputTextProps {
  label: string;
  value: string;
  errorMessage: string | undefined;
  showError: boolean;
  onChange: (value: string) => void;
  onFocusedLost: () => void;
  className?: string;
  placeholder?: string;
}
