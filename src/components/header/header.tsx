import "./header.css";
import DishesImg from "../../images/dishes.jsx";
import { motion } from "framer-motion";

export default function Header() {
  return (
    <header className="header">
      <motion.div
        className="header__logo"
        initial={{ y: 300 }}
        animate={{ y: 0 }}
        transition={{ type: "tween", duration: 0.6 }}
      >
        <DishesImg />
      </motion.div>
    </header>
  );
}
