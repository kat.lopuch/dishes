import { useAppSelector } from "../../../hooks/hooks";
import HexTile from "../../tools/hexTile/hexTile";

export default function AddedSoup() {
  const { postDishResponse } = useAppSelector((state) => state.addedDish);
  if (!postDishResponse) return <></>;

  const { spiciness_scale } = postDishResponse;

  return (
    <HexTile label="spiciness scale" value={spiciness_scale?.toString()} even />
  );
}
