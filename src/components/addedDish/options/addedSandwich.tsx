import { useAppSelector } from "../../../hooks/hooks";
import HexTile from "../../tools/hexTile/hexTile";

export default function AddedSandwich() {
  const { postDishResponse } = useAppSelector((state) => state.addedDish);
  if (!postDishResponse) return <></>;

  const { slices_of_bread } = postDishResponse;

  return (
    <HexTile label="slices of bread" value={slices_of_bread?.toString()} even />
  );
}
