import { useAppSelector } from "../../../hooks/hooks";
import HexTile from "../../tools/hexTile/hexTile";

export default function AddedPizza() {
  const { postDishResponse } = useAppSelector((state) => state.addedDish);
  if (!postDishResponse) return <></>;

  const { no_of_slices, diameter } = postDishResponse;

  return (
    <>
      <HexTile label="slices" value={no_of_slices?.toString()} even />
      <HexTile label="diameter" value={diameter?.toString()} />
    </>
  );
}
