import { useAppSelector } from "../../hooks/hooks";
import HexTile from "../tools/hexTile/hexTile";
import AddedPizza from "./options/addedPizza";
import AddedSoup from "./options/addedSoup";
import AddedSandwich from "./options/addedSandwich";
import useIsDishTypeAdded from "../../hooks/useIsDishTypeAdded";
import "./addedDish.css";

export default function AddedDish() {
  const addedDish = useAppSelector((state) => state.addedDish);
  const isPizzaAdded = useIsDishTypeAdded("pizza");
  const isSoupAdded = useIsDishTypeAdded("soup");
  const isSandwichAdded = useIsDishTypeAdded("sandwich");

  if ("error" === addedDish.status)
    return (
      <p className="addedDish__errorMsg">Dish not added. Please try again.</p>
    );

  if (!addedDish.postDishResponse) return <></>;

  return (
    <section className="addedDish">
      <div className="addedDish__container">
        <h1 className="addedDish__title">
          Recently added <br />
          dish
        </h1>
        <div className="addedDish__hexRows">
          <div className="addedDish__hexRow">
            <HexTile label="id" value={addedDish.postDishResponse.id} />
            <HexTile
              label="name"
              value={addedDish.postDishResponse.name}
              even
            />
            <HexTile
              label="preparation time"
              value={addedDish.postDishResponse.preparation_time}
            />
          </div>
          <div className="addedDish__hexRow">
            <HexTile label="type" value={addedDish.postDishResponse.type} />

            {isPizzaAdded && <AddedPizza />}
            {isSoupAdded && <AddedSoup />}
            {isSandwichAdded && <AddedSandwich />}
          </div>
        </div>
      </div>
    </section>
  );
}
