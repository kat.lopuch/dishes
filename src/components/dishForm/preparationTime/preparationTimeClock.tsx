import { useAppDispatch, useAppSelector } from "../../../hooks/hooks";
import { setIsClockOpen, setPreparationTime } from "../../../store/dishSlice";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { MultiSectionDigitalClock } from "@mui/x-date-pickers/MultiSectionDigitalClock";
import { useOnClickOutside } from "usehooks-ts";
import { useRef } from "react";
import dayjs from "dayjs";
import "./preparationTimeClock.css";

const clockColors = {
  "& .MuiMenuItem-root.Mui-selected": {
    backgroundColor: "#543a2b",
  },
  "& .MuiMenuItem-root:hover": {
    backgroundColor: "#dfd1c8",
  },
  "& .MuiMenuItem-root.Mui-selected:hover": {
    backgroundColor: "#997660",
  },
};

export default function PreparationTimeClock() {
  const ref = useRef(null);
  const dispatch = useAppDispatch();
  const preparationTime = useAppSelector(
    (state) => state.dish.inputs.preparationTime
  );

  const padWithLeadingZeros = (num: string) => {
    return String(num).padStart(2, "0");
  };

  const handlePreparationTimeChange = (value: any) => {
    dispatch(
      setPreparationTime({
        preparationTime: `${padWithLeadingZeros(
          value.$H
        )}:${padWithLeadingZeros(value.$m)}:${padWithLeadingZeros(value.$s)}`,
      })
    );
  };

  const handleClickOutsideClock = () => {
    dispatch(setIsClockOpen({ isClockOpen: false }));
  };

  useOnClickOutside(ref, handleClickOutsideClock);

  return (
    <div className="preparationTimeClock" ref={ref}>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DemoContainer
          components={["DigitalClock", "MultiSectionDigitalClock"]}
        >
          <MultiSectionDigitalClock
            className="preparationTimeClock__digitalClock"
            ampm={false}
            views={["hours", "minutes", "seconds"]}
            timeSteps={{ hours: 1, minutes: 1, seconds: 1 }}
            onChange={handlePreparationTimeChange}
            value={dayjs("2023-01-01T" + preparationTime)}
            sx={clockColors}
          />
        </DemoContainer>
      </LocalizationProvider>
    </div>
  );
}
