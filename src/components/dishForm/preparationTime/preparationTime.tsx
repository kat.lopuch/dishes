import { useAppDispatch, useAppSelector } from "../../../hooks/hooks";
import {
  setIsClockOpen,
  setPreparationTime,
  setPreparationTimeWasFocused,
} from "../../../store/dishSlice";
import InputText from "../../tools/inputText/inputText";
import PreparationTimeClock from "./preparationTimeClock";
import useInputBasicInfo from "../../../hooks/useBasicInputInfo";
import "./preparationTime.css";

export default function PreparationTime() {
  const dispatch = useAppDispatch();
  let { value, errorMessage, showError } = useInputBasicInfo("preparationTime");

  const isClockOpen = useAppSelector((state) => state.dish.isClockOpen);

  const handlePreparationTimeFocusLost = () =>
    dispatch(setPreparationTimeWasFocused());

  const handleButtonClick = () => {
    dispatch(setIsClockOpen({ isClockOpen: true }));
    dispatch(setPreparationTimeWasFocused());
  };

  const handlePreparationTimeInputChange = (value: string) => {
    dispatch(setPreparationTime({ preparationTime: value }));
  };

  return (
    <>
      <div className="preparationTime__container">
        <InputText
          className="preparationTime__input"
          label="Preparation time"
          value={value}
          onChange={handlePreparationTimeInputChange}
          errorMessage={errorMessage}
          placeholder="hh:mm:ss"
          onFocusedLost={handlePreparationTimeFocusLost}
          showError={showError}
        />
        <button
          className="preparationTime__clockBtn"
          onClick={handleButtonClick}
        >
          Clock
        </button>
        {isClockOpen && <PreparationTimeClock />}
      </div>
    </>
  );
}
