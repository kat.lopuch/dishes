import { useAppDispatch, useAppSelector } from "../../../hooks/hooks";
import { setType, setTypeWasFocused } from "../../../store/dishSlice";
import useInputBasicInfo from "../../../hooks/useBasicInputInfo";
import "./dishType.css";

export default function DishType() {
  let { value, errorMessage, showError } = useInputBasicInfo("type");
  const possibleDishes = useAppSelector((state) => state.dish.possibleDishes);
  const classErrorMsg = showError
    ? "dishType__error--shown"
    : "dishType__error--hidden";

  const dispatch = useAppDispatch();
  const handleDishTypeFocusLost = () => dispatch(setTypeWasFocused());
  const handleDishTypeChange = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    dispatch(setType({ type: event.target.value }));
  };

  return (
    <div className="dishType__container">
      <label className="dishType__label">Dish type</label>
      <select
        data-testid="dish-type-selector-id"
        className="dishType__input"
        value={value}
        onChange={handleDishTypeChange}
        onBlur={handleDishTypeFocusLost}
      >
        {possibleDishes.map((dish) => (
          <option key={dish.id} value={dish.value}>
            {dish.description}
          </option>
        ))}
      </select>
      <p data-testid="dish-type-error-text-id" className={classErrorMsg}>
        {errorMessage}
      </p>
    </div>
  );
}
