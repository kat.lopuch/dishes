import "@testing-library/jest-dom";
import { fireEvent, screen } from "@testing-library/react";
import { renderWithProviders } from "../../../store/testUtils";
import userEvent from "@testing-library/user-event";
import DishType from "./dishType";

test("selectable options for the dish type", () => {
  renderWithProviders(<DishType />);
  const dishTypeSelector = screen.getByTestId(
    "dish-type-selector-id"
  ) as HTMLSelectElement;
  expect(dishTypeSelector).toBeInTheDocument();

  userEvent.selectOptions(screen.getByTestId("dish-type-selector-id"), [
    "none",
  ]);
  userEvent.selectOptions(screen.getByTestId("dish-type-selector-id"), [
    "Pizza",
  ]);
  userEvent.selectOptions(screen.getByTestId("dish-type-selector-id"), [
    "Soup",
  ]);
  userEvent.selectOptions(screen.getByTestId("dish-type-selector-id"), [
    "Sandwich",
  ]);
});

test("setting dish type", () => {
  renderWithProviders(<DishType />);
  const dishTypeSelector = screen.getByTestId(
    "dish-type-selector-id"
  ) as HTMLSelectElement;
  expect(dishTypeSelector).toBeInTheDocument();
  expect(dishTypeSelector.value).toBe("none");

  fireEvent.change(dishTypeSelector, { target: { value: "soup" } });
  expect(dishTypeSelector.value).toBe("soup");

  fireEvent.change(dishTypeSelector, { target: { value: "pizza" } });
  expect(dishTypeSelector.value).toBe("pizza");

  fireEvent.change(dishTypeSelector, { target: { value: "sandwich" } });
  expect(dishTypeSelector.value).toBe("sandwich");

  fireEvent.change(dishTypeSelector, { target: { value: "wrong value" } });
  expect(dishTypeSelector.value).toBe("none");
});

test("error text when none value is provided", () => {
  renderWithProviders(<DishType />);
  const dishTypeSelector = screen.getByTestId(
    "dish-type-selector-id"
  ) as HTMLSelectElement;
  expect(dishTypeSelector).toBeInTheDocument();

  fireEvent.change(dishTypeSelector, { target: { value: "none" } });
  const dishTypeErrorMsg = screen.getByTestId(
    "dish-type-error-text-id"
  ) as HTMLParagraphElement;
  expect(dishTypeErrorMsg.textContent).toBe("Dish type must be selected.");
});

test("error text when correct value is provided", () => {
  renderWithProviders(<DishType />);
  const dishTypeSelector = screen.getByTestId(
    "dish-type-selector-id"
  ) as HTMLSelectElement;
  expect(dishTypeSelector).toBeInTheDocument();

  fireEvent.change(dishTypeSelector, { target: { value: "pizza" } });
  const dishTypeErrorMsg = screen.getByTestId(
    "dish-type-error-text-id"
  ) as HTMLParagraphElement;
  expect(dishTypeErrorMsg.textContent).toBe("");
});
