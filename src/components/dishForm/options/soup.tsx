import { useAppDispatch, useAppSelector } from "../../../hooks/hooks";
import { setSpicinessScale } from "../../../store/dishSlice";
import PepperImg from "../../../images/pepper.svg";
import PepperGreyImg from "../../../images/pepper_grey.svg";
import useInputBasicInfo from "../../../hooks/useBasicInputInfo";
import "./soup.css";

interface SpicinessPepperProps {
  spicinessValue: number;
}

function SpicinessPepper(props: SpicinessPepperProps) {
  const dispatch = useAppDispatch();
  let { value: currentSpiciness } = useInputBasicInfo("spicinessScale");
  const img =
    props.spicinessValue > parseInt(currentSpiciness)
      ? PepperGreyImg
      : PepperImg;
  const setSpiciness = () =>
    dispatch(
      setSpicinessScale({ spicinessScale: props.spicinessValue.toString() })
    );

  return (
    <div>
      <img
        src={img}
        alt="Hot pepper"
        className="soup__hotPepperImg"
        key={props.spicinessValue}
        onClick={setSpiciness}
      />
      <p className="soup__spicinessScaleLabel">{props.spicinessValue}</p>
    </div>
  );
}

export default function Soup() {
  const { maxSpiciness, minSpiciness } = useAppSelector((state) => state.dish);

  let scale = [];
  for (
    let spicinessValue = minSpiciness;
    spicinessValue <= maxSpiciness;
    spicinessValue++
  ) {
    scale.push(<SpicinessPepper spicinessValue={spicinessValue} />);
  }

  return (
    <div className="soup__container">
      <label className="soup__label">Spiciness scale</label>
      <div className="soup__spicinessScaleContainer">{scale}</div>
    </div>
  );
}
