import {
  setDiameter,
  setDiameterWasFocused,
  setNoOfSlicesWasFocused,
  setNrOfSlice,
} from "../../../store/dishSlice";
import InputText from "../../tools/inputText/inputText";
import useInputBasicInfo from "../../../hooks/useBasicInputInfo";
import { useAppDispatch } from "../../../hooks/hooks";

export default function Pizza() {
  let {
    value: nrOfSlice,
    errorMessage: errorMessageNrOfSlice,
    showError: showErrorNrOfSlice,
  } = useInputBasicInfo("noOfSlices");

  let {
    value: diameter,
    errorMessage: errorMessageDiameter,
    showError: showErrorDiameter,
  } = useInputBasicInfo("diameter");

  const dispatch = useAppDispatch();
  const handleNoOfSlicesFocusLost = () => dispatch(setNoOfSlicesWasFocused());
  const handleDiameterFocusLost = () => dispatch(setDiameterWasFocused());
  const handleSliceNrChange = (value: string) => {
    dispatch(setNrOfSlice({ noOfSlices: value }));
  };
  const handleDiameterChange = (value: string) => {
    dispatch(setDiameter({ diameter: value }));
  };

  return (
    <>
      <InputText
        label="Number of slices"
        value={nrOfSlice}
        errorMessage={errorMessageNrOfSlice}
        onChange={handleSliceNrChange}
        onFocusedLost={handleNoOfSlicesFocusLost}
        showError={showErrorNrOfSlice}
      />
      <InputText
        label="Diameter [cm]"
        value={diameter}
        errorMessage={errorMessageDiameter}
        onChange={handleDiameterChange}
        onFocusedLost={handleDiameterFocusLost}
        showError={showErrorDiameter}
      />
    </>
  );
}
