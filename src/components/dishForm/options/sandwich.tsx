import {
  setSlicesOfBread,
  setSlicesOfBreadWasFocused,
} from "../../../store/dishSlice";
import InputText from "../../tools/inputText/inputText";
import useInputBasicInfo from "../../../hooks/useBasicInputInfo";
import { useAppDispatch } from "../../../hooks/hooks";

export default function Sandwich() {
  let { value, errorMessage, showError } = useInputBasicInfo("slicesOfBread");
  const dispatch = useAppDispatch();
  const handleSlicesOfBreadFocusLost = () =>
    dispatch(setSlicesOfBreadWasFocused());
  const handleSliceOfBreadChange = (value: string) => {
    dispatch(setSlicesOfBread({ slicesOfBread: value }));
  };
  return (
    <InputText
      label="Slices of bread"
      value={value}
      errorMessage={errorMessage}
      onChange={handleSliceOfBreadChange}
      onFocusedLost={handleSlicesOfBreadFocusLost}
      showError={showError}
    />
  );
}
