import "@testing-library/jest-dom";
import { fireEvent, screen } from "@testing-library/react";
import { renderWithProviders } from "../../../store/testUtils";
import DishName from "./dishName";

test("dish name when valid value is provided", () => {
  renderWithProviders(<DishName />);

  const input = screen.getByTestId("input-text-id") as HTMLInputElement;
  expect(input).toBeInTheDocument();
  expect(input.value).toBe("");

  const validDishName = "Valid dish name";
  fireEvent.change(input, { target: { value: validDishName } });
  expect(input.value).toBe(validDishName);
});

test("error text when valid value is provided", () => {
  renderWithProviders(<DishName />);
  const input = screen.getByTestId("input-text-id") as HTMLInputElement;
  expect(input).toBeInTheDocument();

  const validDishName = "Valid dish name";
  fireEvent.change(input, { target: { value: validDishName } });

  const errorElement = screen.getByTestId(
    "error-text-id"
  ) as HTMLParagraphElement;
  expect(errorElement.textContent).toBe("");
});

test("error text when invalid value is provided", () => {
  renderWithProviders(<DishName />);
  const input = screen.getByTestId("input-text-id") as HTMLInputElement;
  expect(input).toBeInTheDocument();

  const invalidDishName = "a";
  fireEvent.change(input, { target: { value: invalidDishName } });

  const errorElement = screen.getByTestId(
    "error-text-id"
  ) as HTMLParagraphElement;
  expect(errorElement.textContent).toBe(
    "Name must be longer than 3 characters."
  );
});
