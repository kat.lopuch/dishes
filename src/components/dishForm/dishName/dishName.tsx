import { setDishName, setNameWasFocused } from "../../../store/dishSlice";
import InputText from "../../tools/inputText/inputText";
import useInputBasicInfo from "../../../hooks/useBasicInputInfo";
import { useAppDispatch } from "../../../hooks/hooks";

export default function DishName() {
  let { value, errorMessage, showError } = useInputBasicInfo("name");

  const dispatch = useAppDispatch();
  const handleNameFocusLost = () => dispatch(setNameWasFocused());
  const handleNameChange = (value: string) => {
    dispatch(setDishName({ name: value.trimStart() }));
  };

  return (
    <InputText
      label="Dish name"
      value={value}
      errorMessage={errorMessage}
      onChange={handleNameChange}
      onFocusedLost={handleNameFocusLost}
      showError={showError}
    />
  );
}
