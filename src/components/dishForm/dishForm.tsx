import DishName from "./dishName/dishName";
import PreparationTime from "./preparationTime/preparationTime";
import DishType from "./dishType/dishType";
import Pizza from "./options/pizza";
import Soup from "./options/soup";
import Sandwich from "./options/sandwich";
import PostDish from "../postDish/postDish";
import ClearButton from "../clearButton/clearButton";
import { motion } from "framer-motion";
import useIsDishTypeSelected from "../../hooks/useIsDishTypeSelected";
import "./dishForm.css";

export default function DishForm() {
  const isPizzaSelected = useIsDishTypeSelected("pizza");
  const isSoupSelected = useIsDishTypeSelected("soup");
  const isSandwichSelected = useIsDishTypeSelected("sandwich");

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) =>
    event.preventDefault();

  return (
    <main className="dishForm__main">
      <motion.form
        className="dishForm__form"
        onSubmit={handleSubmit}
        initial={{ scale: 0 }}
        animate={{ scale: 1 }}
        transition={{ type: "tween", duration: 0.4, delay: 0.2 }}
      >
        <h1 className="dishForm__title">New Dish</h1>
        <DishName />
        <PreparationTime />
        <DishType />
        <motion.div layout initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
          {isPizzaSelected && <Pizza />}
          {isSoupSelected && <Soup />}
          {isSandwichSelected && <Sandwich />}
        </motion.div>
        <motion.section
          className="dishForm__btnsSection"
          layout
          initial={{ opacity: 0.7 }}
          animate={{ opacity: 1 }}
        >
          <ClearButton />
          <PostDish />
        </motion.section>
      </motion.form>
    </main>
  );
}
