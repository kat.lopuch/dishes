import { useAppSelector } from "./hooks";

export default function usePostDishPending() {
  const addedDishStatus = useAppSelector((state) => state.addedDish.status);
  return "pending" === addedDishStatus;
}
