import { useAppSelector } from "./hooks";

export default function useAreDishInputsValid() {
  const inputErrors = useAppSelector((state) => state.dish.inputErrors);
  let dishInputsValid = true;
  Object.entries(inputErrors).forEach((
    [_, inputErrorValue]) => dishInputsValid = dishInputsValid && (inputErrorValue === undefined))

  return dishInputsValid;
}
