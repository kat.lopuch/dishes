import { useAppSelector } from "./hooks";

export default function useIsDishTypeSelected(
  dishType: "none" | "pizza" | "soup" | "sandwich"
) {
  const type = useAppSelector((state) => state.dish.inputs.type);
  return type === dishType;
}
