import { useAppSelector } from "./hooks";

export default function useIsDishTypeAdded(
  dishType: "none" | "pizza" | "soup" | "sandwich"
) {
  const addedDish = useAppSelector((state) => state.addedDish);
  const type = addedDish.postDishResponse?.type;

  return type === dishType;
}
