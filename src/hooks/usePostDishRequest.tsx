import { useAppSelector } from "./hooks";
import useIsDishTypeSelected from "./useIsDishTypeSelected";

export default function usePostDishRequest() {
  const dish = useAppSelector((state) => state.dish.inputs);
  const isPizzaSelected = useIsDishTypeSelected("pizza");
  const isSoupSelected = useIsDishTypeSelected("soup");
  const isSandwichSelected = useIsDishTypeSelected("sandwich");

  let basicData = {
    name: dish.name,
    type: dish.type,
    preparation_time: dish.preparationTime,
  };

  if (isPizzaSelected)
    return {
      ...basicData,
      no_of_slices: parseInt(dish.noOfSlices),
      diameter: parseFloat(dish.diameter),
    };

  if (isSoupSelected)
    return {
      ...basicData,
      spiciness_scale: parseInt(dish.spicinessScale),
    };

  if (isSandwichSelected)
    return {
      ...basicData,
      slices_of_bread: parseInt(dish.slicesOfBread),
    };

  return basicData;
}
