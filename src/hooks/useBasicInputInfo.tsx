import { useAppSelector } from "./hooks";

export default function useInputBasicInfo(
  inputName:
    | "name"
    | "preparationTime"
    | "type"
    | "noOfSlices"
    | "diameter"
    | "spicinessScale"
    | "slicesOfBread"
) {
  let value = useAppSelector((state) => state.dish.inputs[inputName]);
  let errorMessage = useAppSelector(
    (state) => state.dish.inputErrors[inputName]
  );
  let wasFocused = useAppSelector(
    (state) => state.dish.inputWasFocused[inputName]
  );
  const showError = wasFocused && errorMessage !== undefined;

  return { value, errorMessage, showError };
}
