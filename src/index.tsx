import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { setupStore } from "./store/store";
import DishForm from "./components/dishForm/dishForm";
import AddedDish from "./components/addedDish/addedDish";
import Header from "./components/header/header";
import "./index.css";

const container = document.getElementById("root")!;
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <Provider store={setupStore()}>
      <Header />
      <DishForm />
      <AddedDish />
    </Provider>
  </React.StrictMode>
);
