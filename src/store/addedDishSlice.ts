import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import PostDishRequest from "./types/postDishRequest.types";
import PostDishResponse from "./types/postDishResponse.types";
import AddedDish from "./types/addedDish.types";

const addressUrl =
  "https://umzzcc503l.execute-api.us-west-2.amazonaws.com/dishes/";

const initialState: AddedDish = {
  status: "ok",
  postDishResponse: undefined,
};

export const sendDishRequest = createAsyncThunk<
  { postDishResponse: PostDishResponse },
  PostDishRequest
>("addedDish/request", async (postDishRequest: PostDishRequest) => {
  const response = await fetch(addressUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(postDishRequest),
  });

  const responseJson = await response.json();
  return { postDishResponse: responseJson };
});

const addedDishSlice = createSlice({
  name: "addedDish",
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(sendDishRequest.fulfilled, (state, action) => {
        if (!!action.payload.postDishResponse.id) {
          state.status = "ok";
          state.postDishResponse = action.payload.postDishResponse;
        } else {
          state.status = "error";
          state.postDishResponse = undefined;
        }
      })
      .addCase(sendDishRequest.pending, (state) => {
        state.status = "pending";
        state.postDishResponse = undefined;
      })
      .addCase(sendDishRequest.rejected, (state) => {
        state.status = "error";
        state.postDishResponse = undefined;
      });
  },
});

export default addedDishSlice.reducer;
