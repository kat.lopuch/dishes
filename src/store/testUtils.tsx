import React, { PropsWithChildren } from "react";
import { render } from "@testing-library/react";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import dishReducer from "../store/dishSlice";
import addedDishReducer from "../store/addedDishSlice";
import type { RenderOptions } from "@testing-library/react";
import type { PreloadedState } from "@reduxjs/toolkit";
import type { AppStore, RootState } from "../store/store";

interface ExtendedRenderOptions extends Omit<RenderOptions, "queries"> {
  preloadedState?: PreloadedState<RootState>;
  store?: AppStore;
}

export function renderWithProviders(
  ui: React.ReactElement,
  {
    preloadedState = {
      dish: {
        inputs: {
          name: "",
          preparationTime: "00:00:00",
          type: "none",
          noOfSlices: "1",
          diameter: "25.5",
          spicinessScale: "1",
          slicesOfBread: "1",
        },
        inputWasFocused: {
          name: false,
          preparationTime: false,
          type: false,
          noOfSlices: false,
          diameter: false,
          spicinessScale: false,
          slicesOfBread: false,
        },
        inputErrors: {
          name: "Name must be longer than 3 characters.",
          preparationTime: "Preparation time must be specified.",
          type: "Dish type must be selected.",
          noOfSlices: undefined,
          diameter: undefined,
          spicinessScale: undefined,
          slicesOfBread: undefined,
        },
        possibleDishes: [
          { id: 0, value: "none", description: "Please select dish" },
          { id: 1, value: "pizza", description: "Pizza" },
          { id: 2, value: "soup", description: "Soup" },
          { id: 3, value: "sandwich", description: "Sandwich" },
        ],
        minSpiciness: 1,
        maxSpiciness: 10,
        isClockOpen: false,
      },
      addedDish: {
        status: "ok",
        postDishResponse: undefined,
      },
    },
    store = configureStore({
      reducer: { dish: dishReducer, addedDish: addedDishReducer },
      preloadedState,
    }),
    ...renderOptions
  }: ExtendedRenderOptions = {}
) {
  function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
    return <Provider store={store}>{children}</Provider>;
  }

  return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
