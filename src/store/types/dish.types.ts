export default interface Dish {
  inputs: {
    name: string;
    preparationTime: string;
    type: string;
    noOfSlices: string;
    diameter: string;
    spicinessScale: string;
    slicesOfBread: string;
  };
  inputWasFocused: {
    name: boolean;
    preparationTime: boolean;
    type: boolean;
    noOfSlices: boolean;
    diameter: boolean;
    spicinessScale: boolean;
    slicesOfBread: boolean;
  };
  inputErrors: {
    name: string | undefined;
    preparationTime: string | undefined;
    type: string | undefined;
    noOfSlices: string | undefined;
    diameter: string | undefined;
    spicinessScale: string | undefined;
    slicesOfBread: string | undefined;
  };
  possibleDishes: Array<{
    id: number;
    value: string;
    description: string;
  }>;
  maxSpiciness: number;
  minSpiciness: number;

  isClockOpen: boolean;
}
