import PostDishResponse from "./postDishResponse.types";

export default interface AddedDish {
  status: "ok" | "pending" | "error";
  postDishResponse: PostDishResponse | undefined;
}
