import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import moment from "moment";
import Dish from "./types/dish.types";

const initialState: Dish = {
  inputs: {
    name: "",
    preparationTime: "00:00:00",
    type: "none",
    noOfSlices: "1",
    diameter: "25.5",
    spicinessScale: "1",
    slicesOfBread: "1",
  },
  inputWasFocused: {
    name: false,
    preparationTime: false,
    type: false,
    noOfSlices: false,
    diameter: false,
    spicinessScale: false,
    slicesOfBread: false,
  },
  inputErrors: {
    name: "Name must be longer than 3 characters.",
    preparationTime: "Preparation time must be specified.",
    type: "Dish type must be selected.",
    noOfSlices: undefined,
    diameter: undefined,
    spicinessScale: undefined,
    slicesOfBread: undefined,
  },
  possibleDishes: [
    { id: 0, value: "none", description: "Please select dish" },
    { id: 1, value: "pizza", description: "Pizza" },
    { id: 2, value: "soup", description: "Soup" },
    { id: 3, value: "sandwich", description: "Sandwich" },
  ],
  minSpiciness: 1,
  maxSpiciness: 10,
  isClockOpen: false,
};

const regexDecimalNumber = /^[0-9]*(\.[0-9]{0,1})?$/;
const regexIntegerNumber = /^[0-9]*$/;
const regexTimeFormat =
  /^([0-2]?[0-9]?):?([0-5]?[0-9]?):?([0-5]?[0-9]?)$/;

const dishSlice = createSlice({
  name: "dish",
  initialState,
  reducers: {
    setNameWasFocused: (state) => {
      state.inputWasFocused.name = true;
    },
    setPreparationTimeWasFocused: (state) => {
      state.inputWasFocused.preparationTime = true;
    },
    setTypeWasFocused: (state) => {
      state.inputWasFocused.type = true;
    },
    setNoOfSlicesWasFocused: (state) => {
      state.inputWasFocused.noOfSlices = true;
    },
    setDiameterWasFocused: (state) => {
      state.inputWasFocused.diameter = true;
    },
    setSpicinessScaleWasFocused: (state) => {
      state.inputWasFocused.spicinessScale = true;
    },
    setSlicesOfBreadWasFocused: (state) => {
      state.inputWasFocused.slicesOfBread = true;
    },

    setDishName: (state, action: PayloadAction<{ name: string }>) => {
      state.inputs.name = action.payload.name;
      state.inputErrors.name =
        action.payload.name.length < 3
          ? "Name must be longer than 3 characters."
          : undefined;
    },
    setPreparationTime: (
      state,
      action: PayloadAction<{ preparationTime: string }>
    ) => {
      if (!regexTimeFormat.test(action.payload.preparationTime)) return state;
      state.inputs.preparationTime = action.payload.preparationTime;

      const datePrefix = "2023-01-01T";
      const defaultTimeStamp = datePrefix + initialState.inputs.preparationTime;
      const selectedTimeStamp = datePrefix + action.payload.preparationTime;

      if (moment(selectedTimeStamp).isSame(defaultTimeStamp))
        state.inputErrors.preparationTime = "Preparation time must be specified.";
      else if (action.payload.preparationTime.length < 8 ||
        !moment(selectedTimeStamp).isAfter(defaultTimeStamp)
      )
        state.inputErrors.preparationTime = "Keep format hh:mm:ss.";
      else
        state.inputErrors.preparationTime = undefined;
    },

    setType: (state, action: PayloadAction<{ type: string }>) => {
      state.inputs.type = action.payload.type;
      state.inputErrors.type =
        action.payload.type !== "none"
          ? undefined
          : "Dish type must be selected.";
    },
    setNrOfSlice: (state, action: PayloadAction<{ noOfSlices: string }>) => {
      if (!regexIntegerNumber.test(action.payload.noOfSlices)) return state;
      state.inputs.noOfSlices = action.payload.noOfSlices;
      const noOfSlices = parseInt(action.payload.noOfSlices);
      state.inputErrors.noOfSlices =
        noOfSlices > 0 ? undefined : "At least 1 slice is required.";
    },
    setDiameter: (state, action: PayloadAction<{ diameter: string }>) => {
      if (!regexDecimalNumber.test(action.payload.diameter)) return state;
      state.inputs.diameter = action.payload.diameter;
      const diameter = parseFloat(action.payload.diameter);
      state.inputErrors.diameter =
        diameter > 0 ? undefined : "Diameter bigger than 0 is required.";
    },
    setSpicinessScale: (
      state,
      action: PayloadAction<{ spicinessScale: string }>
    ) => {
      if (!regexDecimalNumber.test(action.payload.spicinessScale)) return state;
      state.inputs.spicinessScale = action.payload.spicinessScale;
      const spicinessScale = parseInt(action.payload.spicinessScale);
      const isInRange =
        spicinessScale <= state.maxSpiciness &&
        spicinessScale >= state.minSpiciness;
      state.inputErrors.spicinessScale = isInRange
        ? undefined
        : `Spiciness must be in the range [${state.maxSpiciness}, ${state.minSpiciness}]`;
    },
    setSlicesOfBread: (
      state,
      action: PayloadAction<{ slicesOfBread: string }>
    ) => {
      if (!regexIntegerNumber.test(action.payload.slicesOfBread)) return state;
      state.inputs.slicesOfBread = action.payload.slicesOfBread;
      const slicesOfBread = parseInt(action.payload.slicesOfBread);
      state.inputErrors.slicesOfBread =
        slicesOfBread > 0 ? undefined : "At least 1 slice is required.";
    },
    setIsClockOpen: (
      state,
      action: PayloadAction<{ isClockOpen: boolean }>
    ) => {
      state.isClockOpen = action.payload.isClockOpen;
    },

    clearForm: () => initialState,
  },
});

export const {
  setNameWasFocused,
  setPreparationTimeWasFocused,
  setTypeWasFocused,
  setNoOfSlicesWasFocused,
  setDiameterWasFocused,
  setSpicinessScaleWasFocused,
  setSlicesOfBreadWasFocused,

  setDishName,
  setPreparationTime,
  setType,
  setNrOfSlice,
  setDiameter,
  setSpicinessScale,
  setSlicesOfBread,
  setIsClockOpen,

  clearForm,
} = dishSlice.actions;

export default dishSlice.reducer;
